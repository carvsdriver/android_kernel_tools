#!/bin/bash
#
# CM10.2 kernel building script for the Skyrocket & Hercules devices
# By Car vs Driver
#
# v.0.1 - Initial version
#	- Still work in progress ... doesn't run yet.
# v.0.2 - Operational version.
# v.0.3 - Added input paramaters to suppor skyrocket and hercules builds
#	- Added optional clean paramater
#	- Added input validation
#	- Cleaned up script overall a bit

# Exit on all error conditions
set -e

# Adjust as appropriate
export CROSS_COMPILE=~/cm-10.2/prebuilts/gcc/linux-x86/arm/arm-eabi-4.7/bin/arm-eabi-

#You shouldn't need to change this
export ARCH=arm

# Validate input parameters
mk_device=$1
mk_dir=$2
mk_option=$3

# Even though the ramdisk information is exactly the same for these devices,
# it makes sense to ensure that we provide a mechanism to differentiate
# them.  Or .. use this as a guide to support additional devices provided
# that you have the correct ramdisk and supporting paramaters below.
# Finally, the cmdline parameters may be different for CM10.1, this script
# assumes you are building from a CM10.2 base

case $mk_device in
	"exhilarate")
		mk_ramdisk="zips/exhilarate-4.3-ramdisk.cpio.gz"
                mk_cmdline="androidboot.hardware=qcom usb_id_pin_rework=true no_console_suspend=true zcache"
                mk_pagesize="2048"
		mk_offset="0x01400000"
                mk_baseaddr="0x40400000"
                mk_config="cyanogenmod_exhilarate_defconfig"
		;;
	"skyrocket")
		mk_ramdisk="zips/skyrocket-4.3-ramdisk.cpio.gz"
		mk_cmdline="androidboot.hardware=qcom usb_id_pin_rework=true no_console_suspend=true zcache"
		mk_pagesize="2048"
		mk_offset="0x01400000"
		mk_baseaddr="0x40400000"
		mk_config="cyanogenmod_skyrocket_defconfig"
		;;
	"hercules")
		mk_ramdisk="zips/hercules-4.3-ramdisk.cpio.gz"
                mk_cmdline="androidboot.hardware=qcom usb_id_pin_rework=true no_console_suspend=true zcache"
                mk_pagesize="2048"
		mk_offset="0x01400000"
		mk_baseaddr="0x40400000"
                mk_config="cyanogenmod_hercules_defconfig"
		;;
	*)
		echo "Usage: build.sh exhilarate|skyrocket|hercules <kernel source directory> [clean]"
		echo "  Example: build.sh skyrocket ~/msm8660-common clean"
		exit 1
		;;
esac

if ! [ -d $mk_dir ]
then
	echo "Invalid kernel directory."
	echo "Usage: build.sh exhilarate|skyrocket|hercules <kernel source directory> [clean]"
	echo "  Example: build.sh skyrocket ~/msm8660-common clean"
	exit 1
fi

(
cd $mk_dir

if ! [ -z $mk_option ]
then
	if [ $mk_option == "clean" ]
	then
		echo "Cleaning ..."
		make clean
		make mrproper
	fi
fi

echo "Running kernel build: "$mk_config
make $mk_config
make -j9

echo "Building kernel modules ..."
make modules

# make the output directory and copy the files we need
if [ -d "out" ]
then
	echo "Removing old out directory."
	rm -rf out
fi

echo "Creating new out direcotireds."
mkdir -p out
mkdir -p out/system
mkdir -p out/system/lib
mkdir -p out/system/lib/modules

echo "Copying build objets to out."
cp arch/arm/boot/zImage out/zImage
find . -name "*.ko" -exec cp {} out/system/lib/modules \;
)

# Now, build the appropriate boot.img file from zImage and ramdisk 
echo "Making boot.img."
./mkbootimg --kernel "$mk_dir"/out/zImage --ramdisk "$mk_ramdisk" --cmdline "$mk_cmdline" --ramdisk_offset "$mk_ofset" --base "$mk_baseaddr" --pagesize "$mk_pagesize" --output "$mk_dir"/out/boot.img

# Now pack up the boot.img and kernel objects into a flashable zip
echo "Making flashable zip."
cp zips/kernel.zip "$mk_dir"/out/"$mk_device"_kernel.zip
cd $mk_dir
cd "$mk_dir"/out
rm zImage
zip -r "$mk_device"_kernel.zip *

echo "Done."

