**STILL TESTING TOOLS - DO NOT USE YET**
=======================



**Kernel building tools**
=======================

**build.sh** - this script will basically do everything for you and is pre-configured to build for the Samsung Hercules, Skyrocket & Exhilarate devices.

	Usage: build.sh skyrocket|hercules <kernel source directory> [clean]
	Example: build.sh skyrocket ~/msm8660-common clean

The output of build.sh, assuming your build succeeds will be a flashable zip located in a new directory named **out/** located in the <kernel source directory> provided when you ran the script.

**Setting up your directories for building**

	Starting in your home:
	$> mkdir android_kernel_tools
	$> cd android_kernel_tools
	android_kernel_tools$> git init
	android_kernel_tools$> git pull https://bitbucket.org/carvsdriver/android_kernel_tools.git

Now that you have kernel tools and your kernel source in seperate directories, you're all ready to build using build.sh.

**For devices other than Hercules, Skyrocket & Exhilarate, you will need to change the updater-script within kernel.zip to ensure it installs boot.img to the correct volume on your desired device.**


**Tools below lifted from XDA:**
=======================

*Source: http://forum.xda-developers.com/showthread.php?t=2319018*

You will not need to use any of these scripts to build your kernel, but I put them in here so that you can create ramdisks and determine mkbootimg parameters as appropriate if you are not building for the Skyrocket or Hercules devices. -CvD

**boot_info** - prints information about the boot.img passed to it, including the base address and ramdisk address. This tool prints out everything needed to repack the boot.img correctly.

	Usage: boot_info (no params, assumes boot.img is in same directory as script)

**split_boot** - More commonly known as split_bootimg.pl, this rips apart the boot.img to extract the ramdisk and zImage. It has been modified by me to split the boot.img into a separate folder (specified by the file name of the boot.img passed to it) and to extract the ramdisk into a sub-folder as well (extracts the cpio from the gz and then extracts the actual files from the cpio archive)

	Usage: split_boot <boot.img file>

**unpack_ramdisk** - unpacks the given ramdisk file.

	Usage: unpack_ramdisk <ramdiskFile>

**repack_ramdisk** - repacks the ramdisk from the given directory (found online and modified slightly to take a directory)

	Usage: repack_ramdisk <ramdiskDirectory> [outputFile]

**mkbootimg** - mkbootimg binary that creates a boot.img file from the given ramdisk and zImage (kernel). Updated to a version compiled by me to support the --ramdiskaddr option (ramdisk address) so that even nonstandard boot.img's can be repacked correctly (Use with boot_info for best results).

	Usage: mkbootimg
		--kernel <filename>
		--ramdisk <filename>
		[ --second <2ndbootloader-filename> ]
		[ --cmdline <kernel-commandline> ]
		[ --board <boardname> ]
		[ --base <address> ]
		[ --pagesize <pagesize> ]
		[ --ramdiskaddr <address> ]
		--output <filename>


**unpack** - wrapper script made for the umkbootimg binary^ to unpack the boot.img into a separate directory and then unpack the ramdisk into a sub-directory.

